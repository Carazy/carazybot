﻿// -----------------------------------------------------------------------
// <copyright file="WaterTests.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Tests.UnitTests.Model
{
    using Ants.Core.Model;

    using NUnit.Framework;

    /// <summary>
    /// This class contains all of the unit tests for the Water object.
    /// </summary>
    public class WaterTests
    {
        /// <summary>
        /// Tests that if two water tiles are located at the same row and column
        /// then they should be equal.  We're testing the GetHashCode() and
        /// Equals implementation
        /// </summary>
        [Test]
        public void TwoWaterTilesAreEqualWhenRowAndColumnAreSame()
        {
            var water1 = new Water(12, 12);
            var water2 = new Water(12, 12);

            Assert.AreEqual(water1, water2);
        }
    }
}