﻿// -----------------------------------------------------------------------
// <copyright file="LocationTests.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Tests.UnitTests.Model
{
    using Ants.Core.Model;

    using NUnit.Framework;

    /// <summary>
    /// This class contains all of the unit tests for the Location object.
    /// </summary>
    public class LocationTests
    {
        /// <summary>
        /// Tests that if two tiles are located at the same row and column
        /// then they should be equal.  We're testing the GetHashCode() and
        /// Equals implementation
        /// </summary>
        [Test]
        public void TwoTilesAreEqualWhenRowAndColumnAreSame()
        {
            var tile1 = new Location(1, 2);
            var tile2 = new Location(1, 2);

            Assert.AreEqual(tile1, tile2);
        }
    }
}