﻿Feature: Bot Moves
    In order to play the game
    As the bot
    I need to be able give moves

Scenario: When told to move my ant moves to an open location
    Given a new bot
    And the following turn information
    | Thing | Row | Column | Owner |
    | Ant   | 0   | 0      | 0     |
    And the bot makes the following moves
    | Row | Column | Direction |
    | 0   | 0      | North     |
    When the game asks for my moves
    Then The moves contain 
    | Row | Column | Direction |
    | 0   | 0      | North     |

Scenario: When telling multiple ants to move they move as expected
    Given a new bot
    And the following turn information
    | Thing | Row | Column | Owner |
    | Ant   | 0   | 0      | 0     |
    | Ant   | 0   | 1      | 0     |
    | Ant   | 0   | 2      | 0     |
    | Ant   | 0   | 3      | 0     |
    And the bot makes the following moves
    | Row | Column | Direction |
    | 0   | 0      | North     |
    | 0   | 1      | East      |
    | 0   | 2      | South     |
    | 0   | 3      | West      |
    When the game asks for my moves
    Then The moves contain
    | Row | Column | Direction |
    | 0   | 0      | North     |
    | 0   | 1      | East      |
    | 0   | 2      | South     |
    | 0   | 3      | West      |