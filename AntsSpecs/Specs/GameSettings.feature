﻿Feature: Game Settings
    In order to play the game
    There needs to be game settings

Scenario: When a bot gets his first turn he has the correct settings.
    Given a new game 
    And a new bot
    And the following inputted settings
    | Game Setting  | Value |
    | loadtime      | 3000  |
    | turntime      | 1000  |
    | rows          | 43    |
    | cols          | 39    |
    | turns         | 60    |
    | viewradius2   | 77    |
    | attackradius2 | 5     |
    | spawnradius2  | 1     |
    | player_seed   | 7     |
    When the game starts
    Then The bot should have received the following settings
    | Game Setting  | Value |
    | loadtime      | 3000  |
    | turntime      | 1000  |
    | rows          | 43    |
    | cols          | 39    |
    | turns         | 60    |
    | viewradius2   | 77    |
    | attackradius2 | 5     |
    | spawnradius2  | 1     |
    | player_seed   | 7     |