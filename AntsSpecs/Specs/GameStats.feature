﻿Feature: Game Stats
    In order to tell how the game finished
    There needs to be game stats

Scenario: When the game is over you should get the game stats
    Given a new game 
    And the score is 1 to 45
    When the game is finished
    Then The bot should have the following game stats
    | Player | Score |
    | 1      | 1     |
    | 2      | 45    |