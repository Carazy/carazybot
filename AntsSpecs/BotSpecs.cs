﻿// -----------------------------------------------------------------------
// <copyright file="BotSpecs.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Tests.AntsSpecs
{
    using Core;

    /// <summary>
    /// This contains all of the basic properties needed for the various Bot
    /// Specs the intention is to inherit from this for the various bot spec
    /// projects so we can have a common place to do things and be able to
    /// share more specs
    /// </summary>
    public class BotSpecs
    {
        /// <summary>
        /// Gets or sets the bot that you will be using to test.
        /// </summary>
        public static IBot Bot { get; protected set; }
    }
}