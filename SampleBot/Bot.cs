﻿// -----------------------------------------------------------------------
// <copyright file="Bot.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Bots.StarterBot
{
    using System.Collections.Generic;
    using System.Linq;

    using Ants.Core;

    /// <summary>
    /// The Sample bot.  This bot basically just always moves north.
    /// </summary>
    public class Bot : IBot
    {
        /// <summary>
        /// Gets or sets the game settings.  This should get set before the
        /// first call to GetMoves
        /// </summary>
        /// <value>
        /// The game settings.
        /// </value>
        public IGameSettings GameSettings { get; set; }

        /// <summary>
        /// Gets or sets the game stats.  This will only be set after the game
        /// is complete.
        /// </summary>
        /// <value>
        /// The game stats.
        /// </value>
        public IGameStats GameStats { get; set; }

        /// <summary>
        /// This is where the bot determines what it wants to do in a turn.
        /// You should probably move all of your ants that are passed in on
        /// turn.  Use the information in turn to decide how you want to move.
        /// </summary>
        /// <param name="turn">The turn.  This contains all information about
        /// the current turn.</param>
        /// <returns>a collection of moves to make in the given turn.</returns>
        public IEnumerable<Move> GetMoves(ITurn turn)
        {
            var moves = new List<Move>();

            // Get all the ants that I own.
            foreach (var ant in turn.Ants.Where(ant => ant.Owner == 0))
            {
                moves.Add(new Move(ant, Direction.North));
            }

            return moves;
        }
    }
}