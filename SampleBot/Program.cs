﻿// -----------------------------------------------------------------------
// <copyright file="Program.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Bots.StarterBot
{
    using Ants.Core;
    using Ants.Core.InputOutput;

    /// <summary>
    /// This is the entry point of the application
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The entrance method of the application
        /// </summary>
        public static void Main()
        {
            var game = new Game(
                new ConsoleGameInput(new ConsoleInput()),
                new ConsoleGameOutput(new ConsoleOutput()));

            game.Start(new Bot());
        }
    }
}