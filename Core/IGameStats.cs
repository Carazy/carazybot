﻿// ----------------------------------------------------------------------------
// <copyright file="IGameStats.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// ----------------------------------------------------------------------------

namespace Ants.Core
{
    /// <summary>
    /// This is the interface for the game settings
    /// </summary>
    public interface IGameStats
    {
        /// <summary>
        /// Gets the final locations of everything at end of game.
        /// </summary>
        ITurn FinalPositions { get; }
    }
}