﻿// -----------------------------------------------------------------------
// <copyright file="Turn.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    using System.Collections.Generic;

    using Ants.Core.Model;

    /// <summary>
    /// Contains all the information about a turn.
    /// </summary>
    public class Turn : ITurn
    {
        /// <summary>
        /// local field for Water property
        /// </summary>
        private readonly IEnumerable<Water> water;

        /// <summary>
        /// local field for Food property
        /// </summary>
        private readonly IEnumerable<Food> food;

        /// <summary>
        /// local field for Anthills property
        /// </summary>
        private readonly IEnumerable<Anthill> anthills;

        /// <summary>
        /// local field for Ants property
        /// </summary>
        private readonly IEnumerable<Ant> ants;

        /// <summary>
        /// local field for DeadAnts property
        /// </summary>
        private readonly IEnumerable<DeadAnt> deadAnts;

        /// <summary>
        /// Initializes a new instance of the <see cref="Turn"/> class.
        /// </summary>
        /// <param name="water">The water.</param>
        /// <param name="food">The food.</param>
        /// <param name="anthills">The anthills.</param>
        /// <param name="ants">The ants.</param>
        /// <param name="deadAnts">The dead ants.</param>
        public Turn(
            IEnumerable<Water> water,
            IEnumerable<Food> food,
            IEnumerable<Anthill> anthills,
            IEnumerable<Ant> ants,
            IEnumerable<DeadAnt> deadAnts)
        {
            this.water = water;
            this.food = food;
            this.anthills = anthills;
            this.ants = ants;
            this.deadAnts = deadAnts;
        }

        /// <summary>
        /// Gets all of the ant hills.
        /// </summary>
        public IEnumerable<Anthill> Anthills
        {
            get
            {
                return this.anthills;
            }
        }

        /// <summary>
        /// Gets the dead ants that are visible on the map.
        /// </summary>
        public IEnumerable<DeadAnt> DeadAnts
        {
            get
            {
                return this.deadAnts;
            }
        }

        /// <summary>
        /// Gets the ants that are visible on the map.
        /// </summary>
        public IEnumerable<Ant> Ants
        {
            get
            {
                return this.ants;
            }
        }

        /// <summary>
        /// Gets the water that is visible on the map.
        /// </summary>
        public IEnumerable<Water> Water
        {
            get
            {
                return this.water;
            }
        }

        /// <summary>
        /// Gets the food that is visible on the map.
        /// </summary>
        public IEnumerable<Food> Food
        {
            get
            {
                return this.food;
            }
        }
    }
}