﻿// ----------------------------------------------------------------------------
// <copyright file="IBot.cs" company="Carazy">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// ----------------------------------------------------------------------------

namespace Ants.Core
{
    using System.Collections.Generic;

    /// <summary>
    /// This is the interface for the various bots.
    /// </summary>
    public interface IBot
    {
        /// <summary>
        /// Gets or sets the game settings.
        /// </summary>
        /// <value>
        /// The game settings.
        /// </value>
        IGameSettings GameSettings { get; set; }

        /// <summary>
        /// Gets or sets the game stats.  This will only be set after the game
        /// is complete.
        /// </summary>
        /// <value>
        /// The game stats.
        /// </value>
        IGameStats GameStats { get; set; }

        /// <summary>
        /// This is where the bot determines what it wants to do in a turn.
        /// You should probably move all of your ants that are passed in on
        /// turn.  Use the information in turn to decide how you want to move.
        /// </summary>
        /// <param name="turn">The turn.  This contains all information about
        /// the current turn.</param>
        /// <returns>a collection of moves to make in the given turn.</returns>
        IEnumerable<Move> GetMoves(ITurn turn);
    }
}