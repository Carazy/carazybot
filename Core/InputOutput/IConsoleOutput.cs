﻿// ----------------------------------------------------------------------------
// <copyright file="IConsoleOutput.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    /// <summary>
    /// This is the interface for all of the console commands.  This is used so
    /// that we can mock the outputs for testing
    /// </summary>
    public interface IConsoleOutput
    {
        /// <summary>
        /// Writes the output string to the console.
        /// </summary>
        /// <param name="outputString">The output string.</param>
        void WriteLine(string outputString);
    }
}