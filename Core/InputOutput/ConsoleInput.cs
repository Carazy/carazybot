﻿// -----------------------------------------------------------------------
// <copyright file="ConsoleInput.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using System;

    /// <summary>
    /// The console input class.  We are using this and the interface so we can
    /// easily test the ConsoleGameInput class.
    /// </summary>
    public class ConsoleInput : IConsoleInput
    {
        /// <summary>
        /// Reads the next line of characters from the console.
        /// </summary>
        /// <returns>The next line of characters from the input stream, or
        /// Nothing if no more lines are available.</returns>
        public string ReadLine()
        {
            return Console.In.ReadLine();
        }
    }
}