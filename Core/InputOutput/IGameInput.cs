﻿// -----------------------------------------------------------------------
// <copyright file="IGameInput.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using Ants.Core;

    /// <summary>
    /// This is the interface for the game input.  On the core this will
    /// probably only end up being for the console.
    /// </summary>
    public interface IGameInput
    {
        /// <summary>
        /// Gets a value indicating whether this instance has another turn.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has next turn; otherwise, <c>false</c>.
        /// </value>
        bool HasNextTurn { get; }

        /// <summary>
        /// Gets the settings for the game.
        /// </summary>
        /// <returns>the settings for the game</returns>
        IGameSettings GetSettings();

        /// <summary>
        /// Gets the turn.  This is advancing the turns every time so you should
        /// really only go through this once.
        /// </summary>
        /// <returns>The current turn.</returns>
        ITurn GetNextTurn();

        /// <summary>
        /// Gets the stats of a finished game.  This will only return something
        /// when the game is over.
        /// </summary>
        /// <returns>null, if the game isn't over but the stats of the game if
        /// it is.</returns>
        IGameStats GetStats();
    }
}