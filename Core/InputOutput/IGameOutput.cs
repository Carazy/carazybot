﻿// -----------------------------------------------------------------------
// <copyright file="IGameOutput.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using System.Collections.Generic;

    using Ants.Core;

    /// <summary>
    /// This is the interface for the game output sources.  On the core this will
    /// probably only end up being for the console.
    /// </summary>
    public interface IGameOutput
    {
        /// <summary>
        /// Posts the moves that are passed in
        /// </summary>
        /// <param name="moves">The moves that you want to be outputted.</param>
        void PostMoves(IEnumerable<Move> moves);

        /// <summary>
        /// Ends the turn.
        /// </summary>
        void EndTurn();
    }
}