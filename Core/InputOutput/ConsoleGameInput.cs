﻿// -----------------------------------------------------------------------
// <copyright file="ConsoleGameInput.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Ants.Core;
    using Ants.Core.Model;

    /// <summary>
    /// This is the interface for the game input.  On the core this will
    /// probably only end up being for the console.
    /// </summary>
    public class ConsoleGameInput : IGameInput
    {
        /// <summary>
        /// The console input.  This is where the actual Console.ReadLine junk
        /// is.  We split it out to a seperate interface for testing.
        /// </summary>
        private readonly IConsoleInput consoleInput;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleGameInput"/>
        /// class.
        /// </summary>
        /// <param name="consoleInput">The console input.</param>
        public ConsoleGameInput(IConsoleInput consoleInput)
        {
            this.consoleInput = consoleInput;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has another turn.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has next turn; otherwise,
        /// <c>false</c>.
        /// </value>
        public bool HasNextTurn
        {
            get
            {
                return this.consoleInput.ReadLine().StartsWith("turn");
            }
        }

        /// <summary>
        /// Gets the settings for the game.
        /// </summary>
        /// <returns>the settings for the game</returns>
        public IGameSettings GetSettings()
        {
            var settingsFromConsole = this.ReadSettingsFromConsole();

            return this.ParseSettingsFromConsole(settingsFromConsole);
        }

        /// <summary>
        /// Gets the turn.  This is advancing the turns every time so you should
        /// really only go through this once.
        /// </summary>
        /// <returns>The current turn.</returns>
        public ITurn GetNextTurn()
        {
            var turnFromConsole = this.ReadTurnFromConsole();

            return this.ParseTurnFromConsole(turnFromConsole);
        }

        /// <summary>
        /// Gets the stats of a finished game.  This will only return something
        /// when the game is over.
        /// </summary>
        /// <returns>null, if the game isn't over but the stats of the game if
        /// it is.</returns>
        public IGameStats GetStats()
        {
            return null;
        }

        /// <summary>
        /// Reads the settings from the console.  Basically just loop through
        /// the junk until the ready string shows up.
        /// </summary>
        /// <returns>the settings as string exactly how they are from console....
        /// well except lowercased if they had any capitalization in them.</returns>
        private IEnumerable<string> ReadSettingsFromConsole()
        {
            return this.ReadFromConsole("ready");
        }

        /// <summary>
        /// Reads the turn from the console.  Basically just loop through
        /// the junk until the go string shows up.
        /// </summary>
        /// <returns>the turn info as string exactly how they are from console....
        /// well except lowercased if they had any capitalization in them.</returns>
        private IEnumerable<string> ReadTurnFromConsole()
        {
            return this.ReadFromConsole("go");
        }

        /// <summary>
        /// Reads from the console.
        /// </summary>
        /// <param name="seperator">The seperator that splits up the group.
        /// this is probably going to be ready or go</param>
        /// <returns>the read lines from the console</returns>
        private IEnumerable<string> ReadFromConsole(string seperator)
        {
            IList<string> lines = new List<string>();
            string line;

            do
            {
                line = this.consoleInput.ReadLine().ToLower(CultureInfo.InvariantCulture);
                if (line != seperator)
                {
                    lines.Add(line);
                }
            }
            while (line != seperator);

            return lines;
        }

        /// <summary>
        /// Parses the settings from console.
        /// </summary>
        /// <param name="settingsFromConsole">The settings from console.</param>
        /// <returns>the game settings in their concrete type format</returns>
        private IGameSettings ParseSettingsFromConsole(IEnumerable<string> settingsFromConsole)
        {
            int totalTurns = 0;
            int turnTime = 0;
            int loadTime = 0;
            int foodGatheringRadius = 0;
            int attackRadius = 0;
            int viewRadius = 0;
            int playerSeed = 0;
            int columns = 0;
            int rows = 0;

            foreach (var line in settingsFromConsole)
            {
                string[] tokens = line.Split();
                string key = tokens[0];
                string value = tokens[1];

                switch (key)
                {
                    // amount of total turns
                    case "turns":
                        totalTurns = int.Parse(value);
                        break;

                    case "cols":
                        columns = int.Parse(value);
                        break;

                    case "rows":
                        rows = int.Parse(value);
                        break;

                    case "player_seed":
                        playerSeed = int.Parse(value);
                        break;

                    case "turntime":
                        turnTime = int.Parse(value);
                        break;

                    case "loadtime":
                        loadTime = int.Parse(value);
                        break;

                    case "attackradius2":
                        attackRadius = int.Parse(value);
                        break;

                    case "viewradius2":
                        viewRadius = int.Parse(value);
                        break;

                    case "spawnradius2":
                        foodGatheringRadius = int.Parse(value);
                        break;

                    // ignored
                    case "turn":
                        break;

                    default:
                        throw new ArgumentException(string.Format("unknown key {0} in settings", key));
                }
            }

            IGameSettings gameSettings = new GameSettings(
                totalTurns,
                turnTime,
                loadTime,
                foodGatheringRadius,
                attackRadius,
                viewRadius,
                playerSeed,
                columns,
                rows);

            return gameSettings;
        }

        /// <summary>
        /// Parses the turn from console.
        /// </summary>
        /// <param name="turnFromConsole">The turn info from console.</param>
        /// <returns>the turn info in it's concrete type</returns>
        private ITurn ParseTurnFromConsole(IEnumerable<string> turnFromConsole)
        {
            IList<Ant> ants = new List<Ant>();
            IList<Food> food = new List<Food>();
            IList<Anthill> anthills = new List<Anthill>();
            IList<DeadAnt> deadAnts = new List<DeadAnt>();
            IList<Water> water = new List<Water>();

            foreach (var line in turnFromConsole)
            {
                string[] tokens = line.Split();
                var row = int.Parse(tokens[1]);
                var column = int.Parse(tokens[2]);

                switch (tokens[0])
                {
                    case "a":
                        ants.Add(new Ant(row, column, int.Parse(tokens[3])));
                        break;

                    case "f":
                        food.Add(new Food(row, column));
                        break;

                    case "h":
                        anthills.Add(new Anthill(row, column, int.Parse(tokens[3])));
                        break;

                    case "d":
                        deadAnts.Add(new DeadAnt(row, column, int.Parse(tokens[3])));
                        break;

                    case "w":
                        water.Add(new Water(row, column));
                        break;

                    default:
                        throw new ArgumentException(
                            "Unknown tile item given (or first token/part of string we don't know about) First Token is "
                            + tokens[0] + " full line is " + line);
                }
            }

            var turn = new Turn(water, food, anthills, ants, deadAnts);

            return turn;
        }
    }
}