﻿// -----------------------------------------------------------------------
// <copyright file="IConsoleInput.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    /// <summary>
    /// This is the interface for all of the console commands.  This is used so
    /// that we can mock the reads for testing
    /// </summary>
    public interface IConsoleInput
    {
        /// <summary>
        /// Reads the next line of characters from the console.
        /// </summary>
        /// <returns>The next line of characters from the input stream, or
        /// Nothing if no more lines are available.</returns>
        string ReadLine();
    }
}