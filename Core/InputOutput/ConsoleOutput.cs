﻿// -----------------------------------------------------------------------
// <copyright file="ConsoleOutput.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using System;

    /// <summary>
    /// The console input class.  We are using this and the interface so we can
    /// easily test the ConsoleGameInput class.
    /// </summary>
    public class ConsoleOutput : IConsoleOutput
    {
        /// <summary>
        /// Writes the output string to the console.
        /// </summary>
        /// <param name="outputString">The output string.</param>
        public void WriteLine(string outputString)
        {
            Console.Out.WriteLine(outputString);
        }
    }
}