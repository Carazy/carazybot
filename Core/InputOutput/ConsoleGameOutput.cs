﻿// -----------------------------------------------------------------------
// <copyright file="ConsoleGameOutput.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.InputOutput
{
    using System;
    using System.Collections.Generic;

    using Ants.Core;

    /// <summary>
    /// This is the console output for the game.
    /// </summary>
    public class ConsoleGameOutput : IGameOutput
    {
        /// <summary>
        /// The console output.  We are splitting this out so that we can more
        /// easily test.
        /// </summary>
        private readonly IConsoleOutput consoleOutput;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleGameOutput"/>
        /// class.
        /// </summary>
        /// <param name="consoleOutput">The console output.</param>
        public ConsoleGameOutput(IConsoleOutput consoleOutput)
        {
            this.consoleOutput = consoleOutput;
        }

        /// <summary>
        /// Posts the moves that are passed in
        /// </summary>
        /// <param name="moves">The moves that you want to be outputted.</param>
        public void PostMoves(IEnumerable<Move> moves)
        {
            const string MoveTemplate = "o {0} {1} {2}";

            foreach (var move in moves)
            {
                string direction;

                switch (move.Direction)
                {
                    case Direction.North:
                        direction = "N";
                        break;

                    case Direction.South:
                        direction = "S";
                        break;

                    case Direction.East:
                        direction = "E";
                        break;

                    case Direction.West:
                        direction = "W";
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                this.consoleOutput.WriteLine(
                    string.Format(MoveTemplate, move.Location.Row, move.Location.Column, direction));
            }
        }

        /// <summary>
        /// Ends the turn.
        /// </summary>
        public void EndTurn()
        {
            this.consoleOutput.WriteLine("go");
        }
    }
}