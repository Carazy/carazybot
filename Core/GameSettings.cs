﻿// -----------------------------------------------------------------------
// <copyright file="GameSettings.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    using System;

    /// <summary>
    /// Stores all the various game settings.
    /// </summary>
    public class GameSettings : IGameSettings, IEquatable<GameSettings>
    {
        /// <summary>
        /// The total amount of turns
        /// </summary>
        private readonly int totalTurns;

        /// <summary>
        /// the amount of time per turn
        /// </summary>
        private readonly int turnTime;

        /// <summary>
        /// The load time
        /// </summary>
        private readonly int loadTime;

        /// <summary>
        /// The food gathering radius
        /// </summary>
        private readonly int foodGatheringRadius;

        /// <summary>
        /// the attack radius
        /// </summary>
        private readonly int attackRadius;

        /// <summary>
        /// the view radius
        /// </summary>
        private readonly int viewRadius;

        /// <summary>
        /// the player seed this is like for random stuff or something.
        /// </summary>
        private readonly int playerSeed;

        /// <summary>
        /// the amount of columns on the map
        /// </summary>
        private readonly int columns;

        /// <summary>
        /// the amount of rows on the map
        /// </summary>
        private readonly int rows;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameSettings"/> class.
        /// </summary>
        /// <param name="totalTurns">The total turns.</param>
        /// <param name="turnTime">The turn time.</param>
        /// <param name="loadTime">The load time.</param>
        /// <param name="foodGatheringRadius">The food gather radius.</param>
        /// <param name="attackRadius">The attack radius.</param>
        /// <param name="viewRadius">The view radius.</param>
        /// <param name="playerSeed">The player seed.</param>
        /// <param name="columns">The columns.</param>
        /// <param name="rows">The rows.</param>
        public GameSettings(
                int totalTurns,
                int turnTime,
                int loadTime,
                int foodGatheringRadius,
                int attackRadius,
                int viewRadius,
                int playerSeed,
                int columns,
                int rows)
        {
            this.totalTurns = totalTurns;
            this.turnTime = turnTime;
            this.loadTime = loadTime;
            this.foodGatheringRadius = foodGatheringRadius;
            this.attackRadius = attackRadius;
            this.viewRadius = viewRadius;
            this.playerSeed = playerSeed;
            this.columns = columns;
            this.rows = rows;
        }

        /// <summary>
        /// Gets the total turns.
        /// </summary>
        public int TotalTurns
        {
            get
            {
                return this.totalTurns;
            }
        }

        /// <summary>
        /// Gets the turn time.
        /// </summary>
        /// <value>
        /// The turn time.
        /// </value>
        public int TurnTime
        {
            get
            {
                return this.turnTime;
            }
        }

        /// <summary>
        /// Gets the load time.
        /// </summary>
        /// <value>
        /// The load time.
        /// </value>
        public int LoadTime
        {
            get
            {
                return this.loadTime;
            }
        }

        /// <summary>
        /// Gets the Food Gathering radius.
        /// </summary>
        /// <value>
        /// The food gathering radius.
        /// </value>
        /// <remarks>
        /// This is refered to as SpawnRadius2 by the game.
        /// </remarks>
        public int FoodGatheringRadius
        {
            get
            {
                return this.foodGatheringRadius;
            }
        }

        /// <summary>
        /// Gets the attack radius.
        /// </summary>
        public int AttackRadius
        {
            get
            {
                return this.attackRadius;
            }
        }

        /// <summary>
        /// Gets the view radius.
        /// </summary>
        /// <value>
        /// The view radius.
        /// </value>
        public int ViewRadius
        {
            get
            {
                return this.viewRadius;
            }
        }

        /// <summary>
        /// Gets the player seed.
        /// </summary>
        /// <value>
        /// The player seed.
        /// </value>
        public int PlayerSeed
        {
            get
            {
                return this.playerSeed;
            }
        }

        /// <summary>
        /// Gets the amount of columns on the map.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public int Columns
        {
            get
            {
                return this.columns;
            }
        }

        /// <summary>
        /// Gets the amount of rows on the map.
        /// </summary>
        public int Rows
        {
            get
            {
                return this.rows;
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(GameSettings other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return other.totalTurns == this.totalTurns &&
                   other.turnTime == this.turnTime &&
                   other.loadTime == this.loadTime &&
                   other.foodGatheringRadius == this.foodGatheringRadius &&
                   other.attackRadius == this.attackRadius &&
                   other.viewRadius == this.viewRadius &&
                   other.playerSeed == this.playerSeed &&
                   other.columns == this.columns &&
                   other.rows == this.rows;
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param><filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(GameSettings))
            {
                return false;
            }

            return this.Equals((GameSettings)obj);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int result = this.totalTurns;
                result = (result * 397) ^ this.turnTime;
                result = (result * 397) ^ this.loadTime;
                result = (result * 397) ^ this.foodGatheringRadius;
                result = (result * 397) ^ this.attackRadius;
                result = (result * 397) ^ this.viewRadius;
                result = (result * 397) ^ this.playerSeed;
                result = (result * 397) ^ this.columns;
                result = (result * 397) ^ this.rows;
                return result;
            }
        }
    }
}