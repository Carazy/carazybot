﻿// -----------------------------------------------------------------------
// <copyright file="Move.cs" company="Carazy.Com">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    using System;
    using System.Globalization;

    using Ants.Core.Model;

    /// <summary>
    /// This is how we tell an ant to move.
    /// </summary>
    public class Move : IEquatable<Move>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Move"/> class.  This
        /// class does not take into consideration any water or if there is an
        /// ant at this location.  It is simply the class used to define a move.
        /// </summary>
        /// <param name="location">
        /// The location that the ant is currently at.
        /// </param>
        /// <param name="direction">
        /// The direction that we want to move the ant..
        /// </param>
        public Move(ILocation location, Direction direction)
        {
            this.Location = location;
            this.Direction = direction;
        }

        /// <summary>
        /// Gets the location that the ant is at that we want to move.
        /// </summary>
        public ILocation Location { get; private set; }

        /// <summary>
        /// Gets the direction that we want to move the ant.
        /// </summary>
        public Direction Direction { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(
                            CultureInfo.CurrentCulture,
                            "location: {0}, Direction: {1}",
                            this.Location,
                            this.Direction);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is
        /// equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with
        /// this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal
        /// to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != typeof(Move))
            {
                return false;
            }

            return this.Equals((Move)obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(Move other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(other.Location, this.Location) && Equals(other.Direction, this.Direction);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing
        /// algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ((this.Location != null ? this.Location.GetHashCode() : 0) * 397) ^ this.Direction.GetHashCode();
            }
        }
    }
}