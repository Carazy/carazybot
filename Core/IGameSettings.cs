﻿// -----------------------------------------------------------------------
// <copyright file="IGameSettings.cs" company="Carazy.Com">
//   Copyright (c) Carazy.Com All Rights Reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    /// <summary>
    /// This is the interface for the game settings
    /// </summary>
    public interface IGameSettings
    {
        /// <summary>
        /// Gets the total turns.
        /// </summary>
        int TotalTurns { get; }

        /// <summary>
        /// Gets the turn time.
        /// </summary>
        /// <value>
        /// The turn time.
        /// </value>
        int TurnTime { get; }

        /// <summary>
        /// Gets the load time.
        /// </summary>
        /// <value>
        /// The load time.
        /// </value>
        int LoadTime { get; }

        /// <summary>
        /// Gets the Food Gathering radius.
        /// </summary>
        /// <value>
        /// The food gathering radius.
        /// </value>
        /// <remarks>
        /// This is refered to as SpawnRadius2 by the game.
        /// </remarks>
        int FoodGatheringRadius { get; }

        /// <summary>
        /// Gets the attack radius.
        /// </summary>
        int AttackRadius { get; }

        /// <summary>
        /// Gets the view radius.
        /// </summary>
        /// <value>
        /// The view radius.
        /// </value>
        int ViewRadius { get; }

        /// <summary>
        /// Gets the player seed.
        /// </summary>
        /// <value>
        /// The player seed.
        /// </value>
        int PlayerSeed { get; }

        /// <summary>
        /// Gets the amount of columns on the map.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        int Columns { get; }

        /// <summary>
        /// Gets the amount of rows on the map.
        /// </summary>
        int Rows { get; }
    }
}