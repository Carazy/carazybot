﻿// -----------------------------------------------------------------------
// <copyright file="Direction.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    /// <summary>
    /// The directions on a map.  North, South, East, and West.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// The north direction
        /// </summary>
        North,

        /// <summary>
        /// The south direction
        /// </summary>
        South,

        /// <summary>
        /// The east direction
        /// </summary>
        East,

        /// <summary>
        /// The west direction
        /// </summary>
        West
    }
}