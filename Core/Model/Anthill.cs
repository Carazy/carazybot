﻿// -----------------------------------------------------------------------
// <copyright file="AntHill.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// This is where your ants spawn.  At the start of the game you are
    /// given 1 ant.
    /// </summary>
    public class Anthill : Location, IOwnedLocation
    {
        /// <summary>
        /// The owner of the anthill
        /// </summary>
        private readonly int owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="Anthill"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="column">The column.</param>
        /// <param name="owner">The owner.</param>
        public Anthill(int row, int column, int owner)
            : base(row, column)
        {
            this.owner = owner;
        }

        /// <summary>
        /// Gets the owner of the location.
        /// </summary>
        public int Owner
        {
            get
            {
                return this.owner;
            }
        }
    }
}