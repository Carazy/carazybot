﻿// -----------------------------------------------------------------------
// <copyright file="IOwnedLocation.cs" company="Carazy.com">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// This tile has an owner!
    /// </summary>
    public interface IOwnedLocation : ILocation
    {
        /// <summary>
        /// Gets the owner of the tile.
        /// </summary>
        int Owner { get; }
    }
}