﻿// -----------------------------------------------------------------------
// <copyright file="Location.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    using System.Globalization;

    /// <summary>
    /// The tile are the individual pieces of the map.
    /// </summary>
    public class Location : ILocation
    {
        /// <summary>
        /// The read only field behind the row property.
        /// </summary>
        private readonly int row;

        /// <summary>
        /// The read only field behind the column property
        /// </summary>
        private readonly int column;

        /// <summary>
        /// Initializes a new instance of the <see cref="Location"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="column">The column.</param>
        public Location(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        /// <summary>
        /// Gets the column the tile is on.
        /// </summary>
        public int Column
        {
            get
            {
                return this.column;
            }
        }

        /// <summary>
        /// Gets the row the tile is on.
        /// </summary>
        public int Row
        {
            get
            {
                return this.row;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(
                            CultureInfo.CurrentCulture,
                            "Row: {0}, Column: {1}",
                            this.Row,
                            this.Column);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return this.Equals((Location)obj);
        }

        /// <summary>
        /// Determines whether the specified <see cref="Location"/> is equal to this instance.
        /// </summary>
        /// <param name="other">The other tile.</param>
        /// <returns>true if they're equal false if they're not.</returns>
        public bool Equals(Location other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return other.row == this.row && other.column == this.column;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (this.row * 397) ^ this.column;
            }
        }
    }
}