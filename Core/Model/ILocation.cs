﻿// -----------------------------------------------------------------------
// <copyright file="ILocation.cs" company="Carazy.Com">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// This is the location.  Everything is pretty much a location.
    /// </summary>
    public interface ILocation
    {
        /// <summary>
        /// Gets the row that the tile is on.
        /// </summary>
        int Row { get; }

        /// <summary>
        /// Gets the column that the tile is on.
        /// </summary>
        int Column { get; }
    }
}