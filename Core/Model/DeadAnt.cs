﻿// -----------------------------------------------------------------------
// <copyright file="DeadAnt.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// Dead Ants are like regular ants except dead.  This can be good to
    /// keep track of.
    /// </summary>
    public class DeadAnt : Location, IOwnedLocation
    {
        /// <summary>
        /// The owner of the dead ant.  He should be proud.
        /// </summary>
        private readonly int owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadAnt"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="column">The column.</param>
        /// <param name="owner">The owner of the dead ant.</param>
        public DeadAnt(int row, int column, int owner)
            : base(row, column)
        {
            this.owner = owner;
        }

        /// <summary>
        /// Gets the owner of the dead ant.
        /// </summary>
        public int Owner
        {
            get
            {
                return this.owner;
            }
        }
    }
}