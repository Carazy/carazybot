﻿// -----------------------------------------------------------------------
// <copyright file="Ant.cs" company="Carazy.Com">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// The ant is what spawns from the ant hills and is pretty much what you
    /// control.  Use your ant to attack other ants, ant hills, and to gather
    /// food.
    /// </summary>
    public class Ant : Location, IOwnedLocation
    {
        /// <summary>
        /// The owner of the ant
        /// </summary>
        private readonly int owner;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ant"/> class.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="column">The column.</param>
        /// <param name="owner">The owner.</param>
        public Ant(int row, int column, int owner)
            : base(row, column)
        {
            this.owner = owner;
        }

        /// <summary>
        /// Gets the owner of the ant.
        /// </summary>
        public int Owner
        {
            get
            {
                return this.owner;
            }
        }
    }
}