﻿// -----------------------------------------------------------------------
// <copyright file="Food.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// Food is good!  When you take it with a live ant then another ant
    /// will spawn at your Ant hill.
    /// </summary>
    public class Food : Location
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Food"/> class.
        /// </summary>
        /// <param name="row">The row the food is on.</param>
        /// <param name="column">The column the food is on.</param>
        public Food(int row, int column)
            : base(row, column)
        {
        }
    }
}