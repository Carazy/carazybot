﻿// -----------------------------------------------------------------------
// <copyright file="Water.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core.Model
{
    /// <summary>
    /// The tile are the individual pieces of the map.
    /// </summary>
    public class Water : Location
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Water"/> class.  Water
        /// is impassable and unoccupiable
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="column">The column.</param>
        public Water(int row, int column)
            : base(row, column)
        {
        }
    }
}