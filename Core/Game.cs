﻿// -----------------------------------------------------------------------
// <copyright file="Game.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    using Ants.Core.InputOutput;

    /// <summary>
    /// The game!  This is the basis of the whole thing.  This is where it
    /// starts.  This is the game.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// The game input.  This will normally be the console.
        /// </summary>
        private readonly IGameInput gameInput;

        /// <summary>
        /// The game output.  This will normally be the console.
        /// </summary>
        private readonly IGameOutput gameOutput;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="gameInput">The game input source.  Probably for most
        /// things besides tests this will just be the console.</param>
        /// <param name="gameOutput">The game output source.  For the most
        /// part this will be the console.</param>
        public Game(IGameInput gameInput, IGameOutput gameOutput)
        {
            this.gameInput = gameInput;
            this.gameOutput = gameOutput;
        }

        /// <summary>
        /// Starts the game loop!
        /// </summary>
        /// <param name="bot">The bot that you want to use.</param>
        public void Start(IBot bot)
        {
            var settings = this.gameInput.GetSettings();
            bot.GameSettings = settings;
            this.EndTurn();

            while (this.gameInput.HasNextTurn)
            {
                var turn = this.gameInput.GetNextTurn();
                this.gameOutput.PostMoves(bot.GetMoves(turn));
                this.EndTurn();
            }

            // if we want to do anything with stats.
            bot.GameStats = this.gameInput.GetStats();
        }

        /// <summary>
        /// Ends the turn.
        /// </summary>
        private void EndTurn()
        {
            this.gameOutput.EndTurn();
        }
    }
}