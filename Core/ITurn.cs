// -----------------------------------------------------------------------
// <copyright file="ITurn.cs" company="Carazy">
// Copyright (c) Carazy.Com.  All Rights Reserved
// </copyright>
// -----------------------------------------------------------------------

namespace Ants.Core
{
    using System.Collections.Generic;

    using Ants.Core.Model;

    /// <summary>
    /// Contains all the information about a turn.
    /// </summary>
    public interface ITurn
    {
        /// <summary>
        /// Gets all of the ant hills.
        /// </summary>
        IEnumerable<Anthill> Anthills { get; }

        /// <summary>
        /// Gets the dead ants that are visible on the map.
        /// </summary>
        IEnumerable<DeadAnt> DeadAnts { get; }

        /// <summary>
        /// Gets the ants that are visible on the map.
        /// </summary>
        IEnumerable<Ant> Ants { get; }

        /// <summary>
        /// Gets the water that is visible on the map.
        /// </summary>
        IEnumerable<Water> Water { get; }

        /// <summary>
        /// Gets the food that is visible on the map.
        /// </summary>
        IEnumerable<Food> Food { get; }
    }
}