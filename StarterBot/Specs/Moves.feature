﻿Feature: Starter Bot Moves North
    In order to demonstrate a basic bot
    I always move north

# Basic Acceptance Criteria
Scenario: When asked for new moves the bot moves north
    Given a new starter bot
    And the following turn information
    | Thing | Row | Column | Owner |
    | Ant   | 0   | 0      | 0     |
    When the game asks for my moves
    Then The moves contain 
    | Row | Column | Direction |
    | 0   | 0      | North     |

Scenario: When asked for new moves the bot moves north with multiple ants
    Given a new starter bot
    And the following turn information
    | Thing | Row | Column | Owner |
    | Ant   | 0   | 0      | 0     |
    | Ant   | 0   | 1      | 0     |
    When the game asks for my moves
    Then The moves contain 
    | Row | Column | Direction |
    | 0   | 0      | North     |
    | 0   | 1      | North     |

# Issues
Scenario: When asked for new moves the bot moves north even if there is water
    Given a new starter bot
    And the following turn information
    | Thing | Row | Column | Owner |
    | Ant   | 0   | 0      | 0     |
    | Water | 1   | 0      |       |
    When the game asks for my moves
    Then The moves contain 
    | Row | Column | Direction |
    | 0   | 0      | North     |
